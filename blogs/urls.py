
from django.urls import path

from blogs import views

urlpatterns = [
    path('', views.list),
    path('login/', views.mylogin),
    path('logout/', views.mylogout),
    path('signup/', views.mysignup),

    path('<int:id>/', views.detail),
    path('<int:id>/review/', views.review),


]
