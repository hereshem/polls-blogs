from django.shortcuts import render, redirect

# Create your views here.
from blogs.forms import ReviewForm, SignupForm, LoginForm
from blogs.models import Article


def list(req):
    blogs = Article.objects.filter(published=True)
    return render(req, "blogs/index.html", {"blogs":blogs, "len":len(blogs)})

def detail(req, id):
    blog = Article.objects.get(id=id)
    blog.view += 1
    blog.save()
    form = ReviewForm()
    return render(req, "blogs/detail.html", {"blog":blog, "form":form })

def review(req, id):
    if req.method == "POST":
        form = ReviewForm(req.POST)
        myreview = form.save(commit=False)
        myreview.article = Article.objects.get(id=id)
        myreview.user = req.user
        myreview.save()

    return redirect("/blogs/" + str(id) + "/")

def mylogin(req):
    return render(req, "blogs/login.html", {"form":LoginForm()})

def mylogout(req):
    return render(req, "blogs/index.html")

def mysignup(req):
    form = SignupForm()
    return render(req, "blogs/signup.html", {"form":form})