from django.contrib import admin

# Register your models here.
from blogs.models import Article, Review

admin.site.register(Article)
admin.site.register(Review)