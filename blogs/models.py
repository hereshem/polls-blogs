from django.contrib.auth.models import User
from django.db import models

# Create your models here.

class Article(models.Model):
    title       = models.CharField(max_length=250)
    description = models.TextField()
    author      = models.CharField(max_length=250)
    created     = models.DateTimeField(auto_now_add=True)
    modified    = models.DateTimeField(auto_now=True)
    published   = models.BooleanField(default=True)
    image       = models.ImageField(upload_to="article", blank=True)
    view        = models.IntegerField(default=0)

    def __str__(self):
        return self.title

class Review(models.Model):
    rate = models.IntegerField(default=10)
    review = models.TextField(blank=True)
    created = models.DateTimeField(auto_now_add=True)
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.review
