from django.http import HttpResponse
from django.shortcuts import render, redirect

# Create your views here.
from polls import models


def home(req):
    return HttpResponse("<h1>Hello World</h1>")

def list(req):
    questions = models.Question.objects.all()
    context = {"questions":questions, "hello":"there"}
    return render(req, "polls/index.html", context)

def detail(req, id):
    question = models.Question.objects.get(id=id)
    dict = {"question":question}
    return render(req, "polls/detail.html", dict)

def vote(req, id):
    # increase count
    if req.method == "POST":
        selected_choice = req.POST.get("choice")
        if selected_choice:
            question = models.Question.objects.get(id=id)
            choice = question.choice_set.get(id=selected_choice)
            choice.vote += 1
            choice.save()

    return redirect("/polls/" + str(id) + "/")

def a():
    ""

def b():
    ""