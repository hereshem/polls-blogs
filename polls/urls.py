
from django.urls import path

from polls import views

urlpatterns = [
    path('', views.list),
    path('<id>/', views.detail),
    path('<id>/vote/', views.vote),

]
