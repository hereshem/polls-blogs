from django.db import models

# Create your models here.

class Question(models.Model):
    text = models.CharField(max_length=250,)
    created = models.DateTimeField()
    modified = models.DateTimeField()
    view = models.IntegerField(default=0)
    published = models.BooleanField(default=True)

    def __str__(self):
        return self.text

class Choice(models.Model):
    text = models.CharField(max_length=250)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    created = models.DateTimeField()
    vote = models.IntegerField(default=0)

    def __str__(self):
        return self.question.text + " - " + self.text
