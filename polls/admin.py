from django.contrib import admin

# Register your models here.
from polls import models

class ChoiceInline(admin.TabularInline):
    model = models.Choice
    extra = 1

class QuestionAdmin(admin.ModelAdmin):
    # list page
    list_display = ["text", "published", "created", "modified", "view"]
    search_fields = ["text", "view"]
    list_filter = ["published", "created"]

    # detail page
    inlines = [ChoiceInline]
    # fields = ["text", "created", "modified"]
    fieldsets = [
        ["Date Info", {"fields": ["created", "modified"]}],
        ["Description", {"fields": ["text"]}],
        ["Actions", {"fields":["published"]}]
    ]

admin.site.register(models.Question, QuestionAdmin)
# admin.site.register(models.Choice)
